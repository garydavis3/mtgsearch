//
//  ViewController.m
//  MTGSearchApp
//
//  Created by Gary Davis on 4/13/17.
//  Copyright © 2017 Gary Davis. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.title = @"Magic: The Gathering Search";
    
    //Set all the cards in the plist into an array for searching the table
    self.allCards = [NSMutableArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"CardList" ofType:@"plist"]];
    
    self.filteredCards = [[NSMutableArray alloc] init];
    
    //set all the cards info into the cards display on the table
    self.displayedCards = self.allCards;
    
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    self.searchController.searchBar.delegate = self;
    self.searchController.dimsBackgroundDuringPresentation = NO;
    
    [self.searchController.searchBar sizeToFit];
    
    self.cardView.tableHeaderView = self.searchController.searchBar;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)aTableView numberOfRowsInSection:(NSInteger)section {
    return [self.displayedCards count];
}

- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)anIndexPath {
    
    UITableViewCell * cell = [aTableView dequeueReusableCellWithIdentifier:@"CardCell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CardCell"];
    }
    
    cell.textLabel.text = [[self.displayedCards objectAtIndex:anIndexPath.row] valueForKey:@"cardName"];
    
    return cell;
}

//Update the search with the typed in subject on the search bar
- (void)updateSearchResultsForSearchController:(UISearchController *)aSearchController {
    NSLog(@"updateSearchResultsForSearchController");
    
    NSString *searchString = aSearchController.searchBar.text;
    NSLog(@"searchString=%@", searchString);
    
    
    if (![searchString isEqualToString:@""]) {
        [self.filteredCards removeAllObjects];
        for (NSDictionary *card in self.allCards) {
            if ([searchString isEqualToString:@""] || [[card valueForKey:@"cardName"] localizedCaseInsensitiveContainsString:searchString] == YES) {
                NSLog(@"card=%@", card);
                [self.filteredCards addObject:card];
            }
        }
        self.displayedCards = self.filteredCards;
    }
    else {
        self.displayedCards = self.allCards;
    }
    [self.cardView reloadData];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self performSegueWithIdentifier:@"cardSegue" sender:indexPath];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSIndexPath* ip = (NSIndexPath*)sender;
    if([segue.identifier isEqualToString:@"cardSegue"])
    {
        CardViewController* cvc = (CardViewController*)segue.destinationViewController;
        cvc.cardInfo = self.displayedCards[ip.row];
    }
}


@end


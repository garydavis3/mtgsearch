//
//  CardViewController.h
//  MTGSearchApp
//
//  Created by Gary Davis on 4/13/17.
//  Copyright © 2017 Gary Davis. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CardViewController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *cardLabel;
@property (strong, nonatomic) IBOutlet UIImageView *cardImage;
@property (strong, nonatomic) NSDictionary* cardInfo;

@end

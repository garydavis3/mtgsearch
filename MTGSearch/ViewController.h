//
//  ViewController.h
//  MTGSearchApp
//
//  Created by Gary Davis on 4/13/17.
//  Copyright © 2017 Gary Davis. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CardViewController.h"

@interface ViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, UISearchControllerDelegate, UISearchBarDelegate, UISearchResultsUpdating>

@property (strong, nonatomic) IBOutlet UITableView *cardView;
@property (nonatomic, strong) UISearchController * searchController;
@property (nonatomic, strong) NSMutableArray * allCards;
@property (nonatomic, strong) NSMutableArray * filteredCards;
@property (nonatomic, weak) NSArray * displayedCards;
@end

